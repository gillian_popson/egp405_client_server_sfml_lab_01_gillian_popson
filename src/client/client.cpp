#include "../header.h"
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#if LIBCAT_SECURITY==1
#include "SecureHandshake.h" // Include header for secure handshake
#endif

#if defined(_CONSOLE_2)
#include "Console2SampleIncludes.h"
#endif

// We copy this from Multiplayer.cpp to keep things all in one file for this example
unsigned char clientGetPacketIdentifier(RakNet::Packet *p);

//SFML Things
sf::RenderWindow* window;
sf::RectangleShape* sprite;

symbols::ClientState state;
symbols::ClientState foeState;

#ifdef _CONSOLE_2
_CONSOLE_2_SetSystemProcessParams
#endif

bool checkInputC(char theInput[2048]);
void startClient();
void initgame();
void updateRender();
bool isKeyDown(sf::Keyboard::Key);

int main(int argc, char* argv[])
{
	startClient();
}

void startClient()
{
#pragma region CLIENT
#pragma region CLIENT_START_UP

	std::vector<std::string> vec;
	std::vector<std::string> argVector = vec;

	//	RakNet::RakNetStatistics *rss;
	// Pointers to the interfaces of our server and client.
	// Note we can easily have both in the same program
	RakNet::RakPeerInterface *client = RakNet::RakPeerInterface::GetInstance();
	//client->InitializeSecurity(0,0,0,0);
	//RakNet::PacketLogger packetLogger;
	//client->AttachPlugin(&packetLogger);

	// Holds packets
	RakNet::Packet* p;

	// GetPacketIdentifier returns this
	unsigned char packetIdentifier;

	// Just so we can remember where the packet came from
	bool isServer;

	// Record the first client that connects to us so we can pass it to the ping function
	RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;

	// Crude interface

	// Holds user data
	char ip[64], serverPort[30], clientPort[30];

	// A client
	isServer = false;

	//printf("This is a sample implementation of a text based chat client.\n");
	//printf("Connect to the project 'Chat Example Server'.\n");
	//printf("Difficulty: Beginner\n\n");

	// Get our input
	std::string portName = "Client now listening on port(200)";
	puts(portName.c_str());

	/*
	Taking this away cause seems unnecessary for now?
	*/
	//Gets(clientPort, sizeof(clientPort));
	//if (clientPort[0] == 0)
	//strcpy(clientPort, "0");

	/*for (unsigned int i = 0; i < argVector.at(2).size(); i++)
	{
		ip[i] = argVector.at(2).at(i);
	}*/

	std::string ipName = "Connecting to IP(127.0.0.1)";
	puts(ipName.c_str());
	//Gets(ip, sizeof(ip));
	client->AllowConnectionResponseIPMigration(false);
	//if (ipName.at(0) == '0')
	strcpy(ip, "127.0.0.1");


	std::string portConnectName = "Now Connecting to port(200)";
	puts(portConnectName.c_str());


	//Gets(serverPort, sizeof(serverPort));

	/*for (unsigned int i = 0; i < argVector.at(3).size(); i++)
	{
		clientPort[i] = argVector.at(3).at(i);
	}*/


	/*for (unsigned int i = 0; i < argVector.at(4).size(); i++)
	{
		serverPort[i] = argVector.at(4).at(i);
	}*/
	//if (serverPort[0] == 0)
	strcpy(serverPort, "200");

	// Connecting the client is very simple.  0 means we don't care about
	// a connectionValidationInteger, and false for low priority threads
	RakNet::SocketDescriptor socketDescriptor(atoi(clientPort), 0);
	socketDescriptor.socketFamily = AF_INET;
	client->Startup(8, &socketDescriptor, 1);
	client->SetOccasionalPing(true);


#if LIBCAT_SECURITY==1
	char public_key[cat::EasyHandshake::PUBLIC_KEY_BYTES];
	FILE *fp = fopen("publicKey.dat", "rb");
	fread(public_key, sizeof(public_key), 1, fp);
	fclose(fp);
#endif

#if LIBCAT_SECURITY==1
	RakNet::PublicKey pk;
	pk.remoteServerPublicKey = public_key;
	pk.publicKeyMode = RakNet::PKM_USE_KNOWN_PUBLIC_KEY;
	bool b = client->Connect(ip, atoi(serverPort), "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"), &pk) == RakNet::CONNECTION_ATTEMPT_STARTED;
#else
	RakNet::ConnectionAttemptResult car = client->Connect(ip, atoi(serverPort), "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	RakAssert(car == RakNet::CONNECTION_ATTEMPT_STARTED);
#endif

	printf("\nMy IP addresses:\n");
	unsigned int i;
	for (i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, client->GetLocalIP(i));
	}

	printf("My GUID is %s\n", client->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());
	//puts("'quit' to quit. 'stat' to show stats. 'ping' to ping.\n'disconnect' to disconnect. 'connect' to reconnnect. Type to talk.");

	char message[2048];

#pragma endregion


	std::cout << "Welcome to 'SHAPE GAME'!\n";
	std::cout << "When the server is ready, the game will begin\n";
	// Loop for input

	bool gameStarted = false;
	bool myTurn = false;
	bool playerConnected = false;
	bool inputGood = false;
	bool skipInput = true;
	bool skip = true;

	sprite = new sf::RectangleShape(sf::Vector2f(20, 20));

	initgame();

	while (1)
	{
		// This sleep keeps RakNet responsive
#ifdef _WIN32
		Sleep(30);
#else
		usleep(30 * 1000);
#endif
		foeState.init();
		state.init();


		if (gameStarted == false)
		{	
			state.x = sf::Mouse::getPosition(*window).x;
			state.y = sf::Mouse::getPosition(*window).y;
			if (isKeyDown(sf::Keyboard::Escape))
			{
				client->CloseConnection(client->GetGUIDFromIndex(0), true);
				return;
			}

			client->Send((const char *)&state, sizeof(symbols::ClientState) * 2, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

			updateRender();
			//printf("Packet Sent...\n");
		}

		// Get a packet from either the server or the client

		for (p = client->Receive(); p; client->DeallocatePacket(p), p = client->Receive())
		{
			// We got a packet, get the identifier with our handy function
			packetIdentifier = clientGetPacketIdentifier(p);

			// Check if this is a network message packet
			switch (packetIdentifier)
			{
			case ID_DISCONNECTION_NOTIFICATION:
				// Connection lost normally
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_ALREADY_CONNECTED:
				// Connection lost normally
				printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", p->guid);
				break;
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
				break;
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_CONNECTION_LOST\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
				break;
			case ID_CONNECTION_BANNED: // Banned from this server
				printf("We are banned from this server.\n");
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("Connection attempt failed\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				// Sorry, the server is full.  I don't do anything here but
				// A real app should tell the user
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;

			case ID_NEW_INCOMING_CONNECTION:

				//client->Send((const char *) &board, sizeof(board), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				break;

			case ID_INVALID_PASSWORD:
				printf("ID_INVALID_PASSWORD\n");
				break;

				////////////////////////////////////////////
			case symbols::messages::ID_GAME_START:

				puts("GAME STARTING SOON...\n");

				gameStarted = true;

			

				break;
				/////////////////////////////////////////////
			case symbols::messages::ID_CLIENT_MOVE:
				symbols::ClientState* s;
				s = reinterpret_cast<symbols::ClientState*>(p->data);

				foeState.x = s->x;
				foeState.y = s->y;

				//printf("Foe Loc: %i %i\n", foeState.x, foeState.y);

				break;
				/////////////////////////////////////////////
			case ID_CONNECTION_LOST:
				// Couldn't deliver a reliable packet - i.e. the other system was abnormally
				// terminated
				printf("ID_CONNECTION_LOST\n");
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				// This tells the client they have connected
				printf("ID_CONNECTION_REQUEST_ACCEPTED to %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n", client->GetExternalID(p->systemAddress).ToString(true));
				break;
			case ID_CONNECTED_PING:


			case ID_UNCONNECTED_PING:
				printf("Ping from %s\n", p->systemAddress.ToString(true));
				break;
			default:
				// It's a client, so just show the message
				printf("%s\n", p->data);
				break;
			}
		}
	}

	// Be nice and let the server know we quit.
	client->Shutdown(300);

	// We're done with the network
	RakNet::RakPeerInterface::DestroyInstance(client);



#pragma endregion
}

void updateRender()
{
	window->clear(sf::Color::Black);
	sprite->setPosition(sf::Vector2f(state.x, state.y));
	window->draw(*sprite);
	sprite->setPosition(sf::Vector2f(foeState.x, foeState.y));
	window->draw(*sprite);
	window->display();
}

void initgame()
{
	//sf::VideoMode fullscreen = sf::VideoMode::getFullscreenModes().at(0);
	window = new sf::RenderWindow(sf::VideoMode(800, 600), "Shape Game - Client");

	std::cout << "Window Create...\n";
}

bool isKeyDown(sf::Keyboard::Key key)
{
	return sf::Keyboard::isKeyPressed(key);
}

// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
unsigned char clientGetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}
