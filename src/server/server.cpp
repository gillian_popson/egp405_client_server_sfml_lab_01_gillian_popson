#include "../header.h"
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

const int MAX_PLAYER_COUNT = 2;

// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

int main()
{

	int playerCount = 0;
	bool gameStarted = false;
	symbols::GameInitState serverControlState;

	RakNet::RakNetGUID clientGUIDS[2];

	clientGUIDS[0] = RakNet::UNASSIGNED_RAKNET_GUID;
	clientGUIDS[1] = RakNet::UNASSIGNED_RAKNET_GUID;

	symbols::ClientState states[2];

	states[0].init();
	states[1].init();

	auto timeOne = Clock::now();
	auto timeTwo = Clock::now();

	bool canBroadcastAgain;
	const byte MS_BETWEEN_BROADCASTS = 100;

	serverControlState.init();
	serverControlState.start = true;

	//start server games
	#pragma region SERVER
	#pragma region SERVER_START_UP
	// Pointers to the interfaces of our server and client.
	// Note we can easily have both in the same program
	RakNet::RakPeerInterface *server = RakNet::RakPeerInterface::GetInstance();
	//RakNet::RakNetStatistics *rss;
	server->SetIncomingPassword("Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	server->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	//	RakNet::PacketLogger packetLogger;
	//	server->AttachPlugin(&packetLogger);

	#if LIBCAT_SECURITY==1
	cat::EasyHandshake handshake;
	char public_key[cat::EasyHandshake::PUBLIC_KEY_BYTES];
	char private_key[cat::EasyHandshake::PRIVATE_KEY_BYTES];
	handshake.GenerateServerKey(public_key, private_key);
	server->InitializeSecurity(public_key, private_key, false);
	FILE *fp = fopen("publicKey.dat", "wb");
	fwrite(public_key, sizeof(public_key), 1, fp);
	fclose(fp);
	#endif

	// Holds packets
	RakNet::Packet* p;

	// GetPacketIdentifier returns this
	unsigned char packetIdentifier;

	// Record the first client that connects to us so we can pass it to the ping function
	RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;

	// Holds user data
	std::string portString;
	char portS[] = "200";

	// A server
	portString = "Now listening on port (200)";
	puts(portString.c_str());
	//Gets(portstring, sizeof(portstring));

	puts("Starting server.");
	// Starting the server is very simple.  2 players allowed.
	// 0 means we don't care about a connectionValidationInteger, and false
	// for low priority threads
	// I am creating two socketDesciptors, to create two sockets. One using IPV6 and the other IPV4
	RakNet::SocketDescriptor socketDescriptors[2];
	socketDescriptors[0].port = atoi(portS);
	socketDescriptors[0].socketFamily = AF_INET; // Test out IPV4
	socketDescriptors[1].port = atoi(portS);
	socketDescriptors[1].socketFamily = AF_INET6; // Test out IPV6
	bool b = server->Startup(4, socketDescriptors, 2) == RakNet::RAKNET_STARTED;
	server->SetMaximumIncomingConnections(4);
	if (!b)
	{
		printf("Failed to start dual IPV4 and IPV6 ports. Trying IPV4 only.\n");

		// Try again, but leave out IPV6
		b = server->Startup(4, socketDescriptors, 1) == RakNet::RAKNET_STARTED;
		if (!b)
		{
			puts("Server failed to start.  Terminating.");
			exit(1);
		}
	}
	server->SetOccasionalPing(true);
	server->SetUnreliableTimeout(1000);

	DataStructures::List< RakNet::RakNetSocket2* > sockets;
	server->GetSockets(sockets);
	printf("Socket addresses used by RakNet:\n");
	for (unsigned int i = 0; i < sockets.Size(); i++)
	{
		printf("%i. %s\n", i + 1, sockets[i]->GetBoundAddress().ToString(true));
	}

	printf("\nMy IP addresses:\n");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}

	printf("\nMy GUID is %s\n", server->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());
	//puts("'quit' to quit. 'stat' to show stats. 'ping' to ping.\n'pingip' to ping an ip address\n'ban' to ban an IP from connecting.\n'kick to kick the first connected player.\nType to talk.");
	char message[2048];
	#pragma endregion

	//start listening for mesages and broadcasting them
	while (true)
	{
		//game logic
		{
			if (playerCount == 2 && !gameStarted)
			{
				puts("Game starting....\n");
				server->Send((const char*)&serverControlState, sizeof(serverControlState), IMMEDIATE_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				gameStarted = true;
				timeOne = Clock::now();
				timeTwo = Clock::now();
				canBroadcastAgain = true;
			}

			if (gameStarted)
			{
				timeTwo = Clock::now();

				if (std::chrono::duration_cast<std::chrono::milliseconds>(timeTwo - timeOne).count() >= MS_BETWEEN_BROADCASTS)
				{
					canBroadcastAgain = true;
				}

				if (canBroadcastAgain)
				{
					server->Send((const char *)&states[0], sizeof(symbols::ClientState) * 2, HIGH_PRIORITY, RELIABLE_ORDERED, 0, clientGUIDS[1], false);
					server->Send((const char *)&states[1], sizeof(symbols::ClientState) * 2, HIGH_PRIORITY, RELIABLE_ORDERED, 0, clientGUIDS[0], false);
					canBroadcastAgain = false;
				}
			}
		}

		for (p = server->Receive(); p; server->DeallocatePacket(p), p = server->Receive())
		{
			// We got a packet, get the identifier with our handy function
			packetIdentifier = GetPacketIdentifier(p);

			// Check if this is a network message packet
			switch (packetIdentifier)
			{
			case ID_DISCONNECTION_NOTIFICATION:
				// Connection lost normally
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				gameStarted = false;

				if (clientGUIDS[0] == p->guid)
					clientGUIDS[0] = RakNet::UNASSIGNED_RAKNET_GUID;
				if (clientGUIDS[1] == p->guid)
					clientGUIDS[1] = RakNet::UNASSIGNED_RAKNET_GUID;

				playerCount--;
				break;
			case ID_ALREADY_CONNECTED:
				// Connection lost normally
				printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", p->guid);
				break;
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
				break;
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_CONNECTION_LOST\n");
				gameStarted = false;
				playerCount--;
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
				break;
			case ID_CONNECTION_BANNED: // Banned from this server
				printf("We are banned from this server.\n");
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("Connection attempt failed\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				// Sorry, the server is full.  I don't do anything here but
				// A real app should tell the user
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;

			case ID_NEW_INCOMING_CONNECTION:
				printf("new incoming connection at %s\n", p->systemAddress.ToString(true));
				if (playerCount + 1 > MAX_PLAYER_COUNT)
				{
					printf("MORE THAN MAX CLIENTS CONNECTED...\n");
					//deny connection
					//server->CancelConnectionAttempt(p->systemAddress);
					server->CloseConnection(p->systemAddress, true);
				}
				else
				{

					if (clientGUIDS[0] == RakNet::UNASSIGNED_RAKNET_GUID)
						clientGUIDS[0] = p->guid;
					if (clientGUIDS[1] == RakNet::UNASSIGNED_RAKNET_GUID)
						clientGUIDS[1] = p->guid;

					playerCount++;
				}
				printf("Current Player Count: %i\n", playerCount);
				break;

			case ID_INVALID_PASSWORD:
				printf("ID_INVALID_PASSWORD\n");
				break;

				////////////////////////////////////////////
			case symbols::messages::ID_GAME_START:

				break;
				/////////////////////////////////////////////
			case symbols::messages::ID_CLIENT_MOVE:
				symbols::ClientState* s;
				s = reinterpret_cast<symbols::ClientState*>(p->data);

				//std::cout << "Player Update...\n";
				printf("Player Update...\n");
				printf("Player %s is now at X: %i, Y: %i\n", p->systemAddress.ToString(true), s->x, s->y);
				printf("\n");

				if (p->guid == clientGUIDS[0])
				{
					states[0].x = s->x;
					states[0].y = s->y;
				}
				if (p->guid == clientGUIDS[1])
				{
					states[1].x = s->x;
					states[1].y = s->y;
				}

				delete s;

				break;
			case ID_CONNECTION_LOST:
				// Couldn't deliver a reliable packet - i.e. the other system was abnormally
				// terminated
				printf("ID_CONNECTION_LOST\n");
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				// This tells the client they have connected
				printf("ID_CONNECTION_REQUEST_ACCEPTED to %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n", server->GetExternalID(p->systemAddress).ToString(true));
				break;
			case ID_CONNECTED_PING:


			case ID_UNCONNECTED_PING:
				printf("Ping from %s\n", p->systemAddress.ToString(true));
				break;
			default:
				// It's a client, so just show the message
				printf("PACKET HAS NO TYPE\n");
				break;
			}
		}
		//get messages, if client message, broadcast to all clients
	}

}
