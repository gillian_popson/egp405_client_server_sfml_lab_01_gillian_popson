#pragma once

#include <iostream>
#include <vector>

#include "MessageIdentifiers.h"

#include "RakPeerInterface.h"
#include "RakNetStatistics.h"
#include "RakNetTypes.h"
#include "BitStream.h"
#include "RakSleep.h"
#include "PacketLogger.h"
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include "Kbhit.h"
#include <stdio.h>
#include <string.h>
#include "Gets.h"
#include "GetTime.h"

namespace symbols
{

	namespace messages
	{
		enum
		{
			ID_GAME_START = ID_USER_PACKET_ENUM,
			ID_CLIENT_MOVE
		};
	}
	
	#pragma pack(push,1)
	struct ClientState
	{
		long unsigned int m_id;
		int x, y;

		void init()
		{
			m_id = messages::ID_CLIENT_MOVE;
			x = 0;
			y = 0;
		}
	};

	#pragma pack(push,1)
	struct GameInitState
	{
		long unsigned int m_id;
		bool start;

		void init()
		{
			m_id = messages::ID_GAME_START;
		}
	};
}