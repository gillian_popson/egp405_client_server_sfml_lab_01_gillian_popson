cmake_minimum_required (VERSION 2.6)
project(PongOut)
set(EXECUTABLE_NAME "pongout")

include_directories(${CMAKE_SOURCE_DIR}/3rdParty/SFML/include/)

link_directories(${CMAKE_SOURCE_DIR}/3rdParty/SFML/lib)

add_executable(Client src/client/client.cpp src/header.h)
add_executable(Server src/server/server.cpp src/header.h)

target_link_libraries(Client sfml-system sfml-graphics sfml-window sfml-system)
target_link_libraries(Server sfml-system sfml-graphics sfml-window sfml-system)

include_directories(${CMAKE_SOURCE_DIR}/3rdParty/RakNet/src)

find_library(
RAKNET_LIB
NAMES RakNetLibStatic
HINTS ${CMAKE_SOURCE_DIR}/3rdParty/RakNet/lib/
)

message(STATUS ${CMAKE_SOURCE_DIR}/3rdParty/RakNet/lib/)

target_link_libraries(Client ${RAKNET_LIB} Ws2_32.lib)
target_link_libraries(Server ${RAKNET_LIB} Ws2_32.lib)

#add_custom_target(tlocAssetsCopier ALL)
#add_custom_command(TARGET tlocAssetsCopier POST_BUILD
#COMMAND ${CMAKE_COMMAND} -E copy directory
#"${CMAKE_SOURCE_DIR}/assets"
#"${SOLUTION_INSTALL_PATH}/assets")
#{
#	copy();
#}